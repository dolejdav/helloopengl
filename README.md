# HelloOpenGL

## Requirements

- JDK 8+

## Controls

- Move: WSAD
- Move up: R
- Move down: F
- Turn left: J
- Turn right: L
- Look up: I
- Look down: K
