package com.webler.shader;

import com.webler.util.ResourceHelper;

import static org.lwjgl.opengl.GL20.*;

public class Shader {
    private final String vertexSource;
    private final String fragmentSource;
    private int program;

    public Shader(String vertexSource, String fragmentSource) {
        this.vertexSource = vertexSource;
        this.fragmentSource = fragmentSource;
        this.program = GL_NONE;
        init();
    }

    public int getProgram() {
        return program;
    }

    public static Shader loadFromResources(String name) {
        ResourceHelper resourceHelper = new ResourceHelper();
        String vertexSource = resourceHelper.getTextResource("shaders/" + name + ".vert");
        String fragmentSource = resourceHelper.getTextResource("shaders/" + name + ".frag");
        return new Shader(vertexSource, fragmentSource);
    }

    private void init() {
        int vertexShader = 0;
        int fragmentShader = 0;

        vertexShader = compileShader(vertexSource, GL_VERTEX_SHADER);
        fragmentShader = compileShader(fragmentSource, GL_FRAGMENT_SHADER);

        program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);

        System.out.println("Program linked successfully.");

        // Check for errors
        int[] isLinked = new int[1];
        glGetProgramiv(program, GL_LINK_STATUS, isLinked);
        if(isLinked[0] == GL_FALSE) {
            System.out.println(glGetProgramInfoLog(program));
        }
    }

    private int compileShader(String source, int type) {
        int shader = glCreateShader(type);
        glShaderSource(shader, source);
        glCompileShader(shader);
        // Check for errors
        int[] isCompiled = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, isCompiled);
        if(isCompiled[0] == GL_FALSE) {
            glDeleteShader(shader);
            System.out.println(glGetShaderInfoLog(shader));
        }
        return shader;
    }
}
