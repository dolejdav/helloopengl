package com.webler.shader;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL20.*;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShaderInstance {
    private final Shader shader;
    private final List<Uniform> uniforms;
    private final String[] uniformNames;

    public ShaderInstance(Shader shader, String[] uniformNames) {
        this.shader = shader;
        this.uniforms = new ArrayList<>();
        this.uniformNames = uniformNames;
        init();
    }

    public void bind() {
        glUseProgram(shader.getProgram());
    }

    public void unbind() {
        glUseProgram(GL_NONE);
    }

    public <T> void supplyUniform(String name, T value) {
        Uniform uniform = uniforms.stream()
                .filter((Uniform u) -> u.getName().equals(name))
                .findAny()
                .orElse(null);

        if(uniform == null) return;

        int location = uniform.getLocation();
        if(value instanceof Matrix4f) {
            float[] mat = new float[16];
            ((Matrix4f) value).get(mat);
            glUniformMatrix4fv(location, false, mat);
        } else if(value instanceof Vector3f) {
            Vector3f vec = (Vector3f) value;
            glUniform3f(location, vec.x, vec.y, vec.z);
        }

    }

    private void init() {
        bind();
        Arrays.asList(uniformNames).forEach((String name) -> {
            int location = glGetUniformLocation(shader.getProgram(), getUniformLocationName(name));
            uniforms.add(new Uniform(name, location));
        });
        unbind();
    }

    private String getUniformLocationName(String name) {
        StringBuilder nameBuffer = new StringBuilder();
        nameBuffer.append("u_");
        for(int i = 0; i < name.length(); ++i) {
            char ch = name.charAt(i);
            if(ch >= 'A' && ch <= 'Z') {
                nameBuffer.append('_');
                nameBuffer.append(Character.toString(ch).toLowerCase());
            } else {
                nameBuffer.append(ch);
            }
        }
        return nameBuffer.toString();
    }
}