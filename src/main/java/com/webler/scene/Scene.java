package com.webler.scene;

import com.webler.camera.Camera;
import com.webler.light.AmbientLight;
import com.webler.light.DiffuseLight;
import com.webler.mesh.MeshInstance;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class Scene {
    public Camera camera;
    public AmbientLight ambientLight;
    public DiffuseLight diffuseLight;

    private List<MeshInstance> meshInstances;

    public Scene(Camera camera) {
        this.camera = camera;
        ambientLight = new AmbientLight(new Vector3f(0.1f, 0.1f, 0.1f));
        diffuseLight = new DiffuseLight(new Vector3f(0.0f, 32.0f, 0.0f), new Vector3f(1.0f, 1.0f, 1.0f));
        meshInstances = new ArrayList<>();
    }

    public void add(MeshInstance meshInstance) {
        meshInstances.add(meshInstance);
    }

    public void draw() {
        meshInstances.forEach((MeshInstance e) -> {
            e.draw(camera, this);
        });
    }
}
