package com.webler.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ResourceHelper {
    public InputStream getResource(String name) {
        return this.getClass()
                .getClassLoader()
                .getResourceAsStream(name);
    }

    public String getTextResource(String name) {
        StringBuilder textBuilder = new StringBuilder();
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getResource(name), StandardCharsets.UTF_8))) {
            int ch = 0;
            while((ch = bufferedReader.read()) != -1) {
                textBuilder.append((char)ch);
            }
        } catch (Exception e) {
            throw new RuntimeException(Arrays.toString(e.getStackTrace()));
        }
        return textBuilder.toString();
    }
}
