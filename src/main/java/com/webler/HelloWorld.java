package com.webler;

import com.webler.camera.Camera;
import com.webler.camera.CameraMovement;
import com.webler.mesh.Cube;
import com.webler.mesh.Mesh;
import com.webler.mesh.MeshInstance;
import com.webler.mesh.Quad;
import com.webler.scene.Scene;
import com.webler.shader.Shader;
import com.webler.shader.ShaderInstance;
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class HelloWorld {

    // The window handle
    private long window;

    public void run() {
        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        init();
        loop();

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        // Create the window
        window = glfwCreateWindow(720, 480, "Hello World!", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    private void loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        Shader shader = Shader.loadFromResources("default");
        ShaderInstance shaderInstance = new ShaderInstance(shader, new String[]{
                "projectionMatrix",
                "viewMatrix",
                "worldMatrix",
                "ambient",
                "lightPosition",
                "lightColor",
                "color"
        });
        Mesh quad = new Quad();
        Mesh cube = new Cube();
        Camera camera = new Camera((float)Math.PI / 4, 1.0f, 0.1f, 100.0f);
        camera.position.y = 2;
        camera.position.z = 5;
        CameraMovement movement = new CameraMovement(camera, (float)Math.PI / 3);
        Scene scene = new Scene(camera);
        scene.diffuseLight.position.x = -64;
        scene.diffuseLight.position.y = 128;
        scene.diffuseLight.position.z = 96;

        for(int i = 0; i < 10; ++i) {
            for(int j = 0; j < 10; ++j) {
                for (int k = 0; k < 10; ++k) {
                    MeshInstance meshInstance = new MeshInstance(cube, shaderInstance);
                    meshInstance.position.z = i * -4.0f;
                    meshInstance.position.x = j * 4.0f;
                    meshInstance.position.y = k * 4.0f;
                    meshInstance.scale.set(2f, 2f, 2f);
                    meshInstance.color.set((float)Math.random(), (float)Math.random(), (float)Math.random());
                    scene.add(meshInstance);
                }
            }
        }

        // Set the clear color
        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        glEnable(GL_DEPTH_TEST);

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while ( !glfwWindowShouldClose(window) ) {
            int[] width = new int[1];
            int[] height = new int[1];
            glfwGetWindowSize(window, width, height);

            camera.update();
            camera.aspect = (float)width[0] / (float)height[0];
            camera.updateProjection();

            if(glfwGetKey(window, GLFW_KEY_W) == 1) {
                movement.moveForward(0.1f);
            } else if(glfwGetKey(window, GLFW_KEY_S) == 1) {
                movement.moveBackward(0.1f);
            }
            if(glfwGetKey(window, GLFW_KEY_A) == 1) {
                movement.moveLeft(0.1f);
            } else if(glfwGetKey(window, GLFW_KEY_D) == 1) {
                movement.moveRight(0.1f);
            }
            if(glfwGetKey(window, GLFW_KEY_R) == 1) {
                movement.moveUp(0.1f);
            } else if(glfwGetKey(window, GLFW_KEY_F) == 1) {
                movement.moveDown(0.1f);
            }
            if(glfwGetKey(window, GLFW_KEY_J) == 1) {
                movement.rotateYaw(-0.025f);
            } else if(glfwGetKey(window, GLFW_KEY_L) == 1) {
                movement.rotateYaw(0.025f);
            }
            if(glfwGetKey(window, GLFW_KEY_I) == 1) {
                movement.rotatePitch(0.025f);
            } else if(glfwGetKey(window, GLFW_KEY_K) == 1) {
                movement.rotatePitch(-0.025f);
            }

            glViewport(0, 0, width[0], height[0]);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            scene.draw();

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
        }
    }

    public static void main(String[] args) {
        new HelloWorld().run();
    }

}