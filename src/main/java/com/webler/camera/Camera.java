package com.webler.camera;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera {
    private final Matrix4f viewMatrix;
    private final Matrix4f projectionMatrix;
    public final Vector3f position;
    private final Vector3f forward;
    private final Vector3f up;
    private final Vector3f right;
    public float fov;
    public float near;
    public float far;
    public float aspect;
    public float yaw;
    public float pitch;

    public Camera(float fov, float aspect, float near, float far) {
        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;
        viewMatrix = new Matrix4f();
        projectionMatrix = new Matrix4f();
        position = new Vector3f();
        forward = new Vector3f();
        up = new Vector3f(0, 1, 0);
        right = new Vector3f();
        yaw = 0;
        pitch = 0;
        updateProjection();
    }

    public void update() {
        forward.x = (float)Math.cos(pitch) * (float)Math.sin(yaw);
        forward.y = (float)Math.sin(pitch);
        forward.z = (float)Math.cos(pitch) * (float)Math.cos(yaw);

        forward.normalize();
        right.set(new Vector3f(0, 1, 0).cross(forward));
        up.set(new Vector3f(forward).cross(right));

        Vector3f eye = new Vector3f(position).add(forward);
        viewMatrix.identity().lookAt(eye, position, up);
    }

    public void updateProjection() {
        projectionMatrix.identity().perspective(fov, aspect, near, far);
    }

    public Vector3f getPosition() {
        return position;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }
}
