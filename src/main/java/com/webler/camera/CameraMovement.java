package com.webler.camera;

import org.joml.Vector3f;

public class CameraMovement {
    private final Camera camera;
    public float maxPitch;

    public CameraMovement(Camera camera, float maxPitch) {
        this.camera = camera;
        this.maxPitch = maxPitch;

    }

    public void moveForward(float velocity) {
        camera.position.add(new Vector3f(0, 0, -velocity).rotateY(camera.yaw));
    }

    public void moveBackward(float velocity) {
        camera.position.add(new Vector3f(0, 0, velocity).rotateY(camera.yaw));
    }

    public void moveUp(float velocity) {
        camera.position.y += velocity;
    }

    public void moveDown(float velocity) {
        camera.position.y -= velocity;
    }

    public void moveLeft(float velocity) {
        camera.position.add(new Vector3f(-velocity, 0, 0).rotateY(camera.yaw));
    }

    public void moveRight(float velocity) {
        camera.position.add(new Vector3f(velocity, 0, 0).rotateY(camera.yaw));
    }

    public void rotateYaw(float rad) {
        camera.yaw -= rad;
    }

    public void rotatePitch(float rad) {
        camera.pitch -= rad;
        camera.pitch = Math.max(Math.min(camera.pitch, maxPitch), -maxPitch);
    }
}
