package com.webler.light;

import org.joml.Vector3f;

public class AmbientLight {
    public Vector3f color;

    public  AmbientLight(Vector3f color) {
        this.color = color;
    }
}
