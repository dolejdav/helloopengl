package com.webler.light;

import org.joml.Vector3f;

public class DiffuseLight {
    public Vector3f position;
    public Vector3f color;

    public DiffuseLight(Vector3f position, Vector3f color) {
        this.position = position;
        this.color = color;
    }
}
