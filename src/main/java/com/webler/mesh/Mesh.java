package com.webler.mesh;

public abstract class Mesh {
    protected float[] vertices;
    protected int[] indices;

    public Mesh() {
        init();
    }

    public float[] getVertices() {
        return vertices;
    }

    public int[] getIndices() {
        return indices;
    }

    protected abstract void init();
}
