package com.webler.mesh;

import com.webler.camera.Camera;
import com.webler.scene.Scene;
import com.webler.shader.ShaderInstance;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public class MeshInstance {
    private static final int STRIDE = 8;
    private final Mesh mesh;
    private final ShaderInstance shaderInstance;
    private int VAO;
    public final Vector3f position;
    public final Vector3f scale;
    public final Quaternionfc rotation;
    public final Vector3f color;

    public MeshInstance(Mesh mesh, ShaderInstance shaderInstance) {
        this.mesh = mesh;
        this.shaderInstance = shaderInstance;
        position = new Vector3f();
        scale = new Vector3f(1.0f, 1.0f, 1.0f);
        rotation = new Quaternionf();
        color = new Vector3f(1.0f, 1.0f, 1.0f);
        init();
    }

    public void draw(Camera camera, Scene scene) {
        Matrix4f transformMatrix = new Matrix4f().identity()
                .translate(position)
                .rotate(rotation)
                .scale(scale);
        shaderInstance.bind();
        shaderInstance.supplyUniform("projectionMatrix", camera.getProjectionMatrix());
        shaderInstance.supplyUniform("viewMatrix", camera.getViewMatrix());
        shaderInstance.supplyUniform("worldMatrix", transformMatrix);
        shaderInstance.supplyUniform("ambient", scene.ambientLight.color);
        shaderInstance.supplyUniform("lightPosition", scene.diffuseLight.position);
        shaderInstance.supplyUniform("lightColor", scene.diffuseLight.color);
        shaderInstance.supplyUniform("color", color);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, mesh.getIndices().length, GL_UNSIGNED_INT, 0);
        glBindVertexArray(GL_NONE);
        shaderInstance.unbind();
    }

    private void init() {
        VAO = glGenVertexArrays();
        glBindVertexArray(VAO);
        int VBO = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, mesh.getVertices(), GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, STRIDE * Float.BYTES, 0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, STRIDE * Float.BYTES, 3 * Float.BYTES);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(2, 3, GL_FLOAT, false, STRIDE * Float.BYTES, 5 * Float.BYTES);
        glEnableVertexAttribArray(2);
        int EBO = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.getIndices(), GL_STATIC_DRAW);
        glBindVertexArray(GL_NONE);
    }
}
