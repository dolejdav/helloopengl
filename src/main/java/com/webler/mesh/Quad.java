package com.webler.mesh;

public class Quad extends Mesh {
    @Override
    protected void init() {
        vertices = new float[] {
                -0.5f,  0.5f,  0.0f,  0.0f, 0.0f,  0.0f, 0.0f, 1.0f,
                 0.5f,  0.5f,  0.0f,  1.0f, 0.0f,  0.0f, 0.0f, 1.0f,
                 0.5f, -0.5f,  0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 1.0f,
                -0.5f, -0.5f,  0.0f,  0.0f, 1.0f,  0.0f, 0.0f, 1.0f
        };

        indices = new int[] {
                0, 1, 2,
                0, 2, 3
        };
    }
}
