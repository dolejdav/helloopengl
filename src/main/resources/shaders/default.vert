#version 330

layout (location=0) in vec3 a_position;
layout (location=1) in vec2 a_uv;
layout (location=2) in vec3 a_normal;

uniform mat4 u_projection_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_world_matrix;

out vec2 uv;
out vec3 normal;
out vec3 frag_pos;

void main() {
    uv = a_uv;
    normal = mat3(u_world_matrix) * a_normal;
    frag_pos = vec3(u_world_matrix * vec4(a_position, 1.0));
    gl_Position = u_projection_matrix * u_view_matrix * u_world_matrix * vec4(a_position, 1.0);
}