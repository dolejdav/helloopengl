#version 330

/**
 * Light source
 * https://learnopengl.com/Lighting/Basic-Lighting
 */

precision mediump float;

in vec2 uv;
in vec3 normal;
in vec3 frag_pos;

uniform vec3 u_ambient;
uniform vec3 u_light_position;
uniform vec3 u_light_color;
uniform vec3 u_color;

out vec4 fragColor;

float random(in vec2 st) {
    return fract(sin(dot(st, vec2(42.0774, 15.3907))) * 608184);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    vec2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners percentages
    return mix(a, b, u.x) +
    (c - a)* u.y * (1.0 - u.x) +
    (d - b) * u.x * u.y;
}

void main() {
    vec3 light_dir = normalize(u_light_position - frag_pos);
    float diff = max(dot(normalize(normal), light_dir), 0.0);
    vec3 diffuse = diff * u_light_color;
    vec3 result = (u_ambient + diffuse) * u_color * noise(uv * 5.0);
    fragColor = vec4(result, 1.0);
}